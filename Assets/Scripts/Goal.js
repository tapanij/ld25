#pragma strict
// Goal knows when the player finishes the level
class Goal extends MonoBehaviour {
	// We inform the GameController if the player finishes the current level
	function OnTriggerEnter(c: Collider) {
	    if (c.gameObject.tag == "Player") {
	        if (audio) {
	            if (!audio.isPlaying) audio.Play();
	        }
	        SendMessageUpwards("Goal");
	        // Only trigger once
	        collider.enabled = false;
	    }
	}
}