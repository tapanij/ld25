#pragma strict
// PlayerSpawnPoint broadcasts to the game controller when the player reaches the checkpoint
class PlayerSpawnPoint extends MonoBehaviour {
	// The player reaches the checkpoint. Inform the GameObject
	function OnTriggerEnter(c: Collider) {
	    if (c.gameObject.tag == "Player") {
	        if (audio) {
	            if (!audio.isPlaying) audio.Play();
	        }
	        SendMessageUpwards("SpawnPoint", this);
	        collider.enabled = false;
	    }
	}
}