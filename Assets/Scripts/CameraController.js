#pragma strict
// The camera follows the player if the game state is running
class CameraController extends MonoBehaviour {
	public var player: Transform;
	
	function CameraMovement () {
		if(player){
			transform.position = Vector3(player.position.x +9f ,25f,-25f);
		}
	}
}