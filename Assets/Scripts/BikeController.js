#pragma strict
// BikeController makes the bike move and handles bike's collisions
class BikeController extends MonoBehaviour {
	// Is the bike touching the ground?
	private var isColliding: boolean;
	private var acceleration: float = 20f;
	private var maxSpeed : float = -10.0f;
	private var engineRunning: boolean;
	
	function Awake() {
	    isColliding = false;
	    engineRunning = true;
	}
	
	// Update method
	function EnemyMovement() {
	    //	Debug.Log("velocity " + rigidbody.velocity.magnitude);
	    Debug.DrawLine(transform.position, transform.position + transform.up * 10);
	
		// Accelerate the bike if it still has a driver and the wheels are touching something
	    if (engineRunning && isColliding) {
	    	rigidbody.AddForce(transform.up * acceleration, ForceMode.Acceleration);
	    	// Make sure we don't go any faster than maxSpeed
			rigidbody.velocity.x = Mathf.Max (rigidbody.velocity.x, maxSpeed);   	
	    }
	}
	
	// If the bike hit's the player in high velocity then the player is dead, ouch!
	function OnCollisionEnter(collision: Collision) {
	    if (collision.collider.CompareTag("Player") && collision.relativeVelocity.magnitude > 6.5f) {
	        SendMessageUpwards("LostLife");
	    }
	}
	
	function OnCollisionStay(collision: Collision) {
	    if (!isColliding){
	     isColliding = true;
	    }
	}
	
	function OnCollisionExit(collisionInfo: Collision) {
	    isColliding = false;
	}
	
	// The driver lost the bike. Make's sure the bike doesn't move anymore.
	function StopEngine() {
	    engineRunning = false;
	    // Allows the bike to fall in realistic way
	    rigidbody.constraints = RigidbodyConstraints.None;
	    Destroy(gameObject, 5);
	}
}