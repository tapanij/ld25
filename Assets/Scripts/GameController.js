#pragma strict
// GameController takes care of the game states, updates main scripts (player, enemy and AI) and draws GUI 
class GameController extends MonoBehaviour {
	public var state: GameState;
	public enum GameState {
	    LevelRunning,
	    LostLife,
	    Goal
	}
	// The bike prefab
	public var bike: Transform;
	public var bikeSpawnPoint: Transform;
	public var secondBikeSpawnPoint: Transform;
	// Player's start position
	public var firstPlayerSpawnPoint: PlayerSpawnPoint;
	// Player's current checkpoint
	private var currentPlayerSpawnPoint: PlayerSpawnPoint;
	public var player: ThirdPersonController;
	public var spawnBikeInterval: float;
	public var style: GUIStyle;
	public var font: Font;
	private var subtitle: int;
	public var playerRagdoll: Transform;
	
	function Start() {
	    StateChange(GameState.LevelRunning);
	    currentPlayerSpawnPoint = firstPlayerSpawnPoint;
	    style = new GUIStyle();
	    style.font = font;
	    subtitle = 0;
	}
	
	function Awake() {
	    InvokeRepeating("SpawnBike", 2, spawnBikeInterval);
	}
	
	function Update() {
		// Quit android application
	    if (Input.GetKey(KeyCode.Escape))
	    {
	       Application.Quit();
	    }
	
	    switch (state) {
	        case GameState.LevelRunning:
	        	// Player input
	            BroadcastMessage("PlayerMovement");
	            // Update camera position
	            BroadcastMessage("CameraMovement");
	            break;
	    }
	}
	
	function FixedUpdate() {
	    switch (state) {
	        case GameState.LevelRunning:
	        	// Update drivers 
	            BroadcastMessage("EnemyMovement");
	            break;
	    }
	}
	
	// The game state changed
	function StateChange(newState: GameState) {
	    switch (newState) {
	    	// The player died
	        case GameState.LostLife:
	            // Create a player ragdoll to player's position
	            var clonedRagdoll: Transform;
	            clonedRagdoll = Instantiate(playerRagdoll, player.transform.position, player.transform.rotation);
	     		
	     		// Move the actual player below surface, but don't destroy it
	            player.collider.isTrigger = true;
	            player.transform.position = Vector3(player.transform.position.x, -100f, player.transform.position.z);
				
				// Tell the ThirdPersonController to drop the item if the player is carrying something
	 			BroadcastMessage("DropItem");
	 			// Spawn the player to a checkpoint after delay
	            Invoke("SpawnPlayer", 3);
	            break;
	
			// The player finished the level
	        case GameState.Goal:
	        	// GUI text on
	            subtitle = 1;
	            
	            // Stop spawning bikes
	            CancelInvoke();
	            
	            // Did the player beat the game yet? If not then load the next level
	            if (Application.loadedLevel != 6) {
	                Invoke("ChangeScene", 4);
	            }
	            break;
	    }
	    state = newState;
	}
	
	// Spawn the player to the current checkpoint
	function SpawnPlayer() {
	    player.transform.position = currentPlayerSpawnPoint.transform.position;
	    player.collider.isTrigger = false;
	    StateChange(GameState.LevelRunning);
	}
	
	function SpawnBike() {
	    var instance: Transform;
	    if (bikeSpawnPoint) {
	        instance = Instantiate(bike, bikeSpawnPoint.transform.position, bikeSpawnPoint.transform.rotation);
	        // Make the bike as GameController's child so we can broadcast messages to it
	        instance.transform.parent = transform;
	    }
	
		// Spawn another bike if the current level has a second bike spawn point
	    if (secondBikeSpawnPoint) {
	        instance = Instantiate(bike, secondBikeSpawnPoint.transform.position, secondBikeSpawnPoint.transform.rotation);
	        instance.transform.parent = transform;
	    }
	}
	
	// The player reached a new checkpoint. Update the current player spawn point
	function SpawnPoint(sp: PlayerSpawnPoint) {
	    currentPlayerSpawnPoint = sp;
	}
	
	// The player finished the level
	function Goal() {
	    StateChange(GameState.Goal);
	}
	
	// Change to the next level
	function ChangeScene() {
	    Application.LoadLevel(Application.loadedLevel + 1);
	}
	
	function OnGUI() {
	    if (subtitle == 1) {
	    	// The player finished the level
	        if (Application.loadedLevel != 6) {
	            GUI.Label(new Rect(Screen.width * 0.5, Screen.height * 0.5, 800, 40), "Well done!", style);
	            GUI.Label(new Rect(Screen.width * 0.5, Screen.height * 0.6, 800, 40), "Level " + (Application.loadedLevel + 1) + " next", style);
	        } else {
	        // The player finished the last level
	            GUI.Label(new Rect(Screen.width * 0.5, Screen.height * 0.5, 800, 40), "Game over!", style);
	            GUI.Label(new Rect(Screen.width * 0.5, Screen.height * 0.6, 800, 40), "Thanks for playing!", style);
	        }
	    }
	}
	
	function LostLife(){
		StateChange(GameState.LostLife);
	}
	
	//function SubtitleOff ()
	//{
	//	subtitle = 2;
	//}
}