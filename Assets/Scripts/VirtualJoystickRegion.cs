using UnityEngine;

public class VirtualJoystickRegion : MonoBehaviour
{

	public static Vector2 VJRvector;    // Joystick's controls in Screen-Space.

	public static Vector2 VJRnormals;   // Joystick's normalized controls.

	public static bool VJRdoubleTap;    // Player double tapped this Joystick.

	public Color activeColor;           // Joystick's color when active.

	public Color inactiveColor;         // Joystick's color when inactive.

	public Texture2D joystick2D;        // Joystick's Image.

	public Texture2D background2D;      // Background's Image.

	private GameObject backOBJ;         // Background's Object.
	
	// @author Tapani
	private GameObject joystickOBJ;         // Joystick's Object.

	private GUITexture joystick;        // Joystick's GUI.

	private GUITexture background;      // Background's GUI.

	private Vector2 origin;             // Touch's Origin in Screen-Space.

	private Vector2 position;           // Pixel Position in Screen-Space.

	private int size;                   // Screen's smaller side.

	private float length;               // The maximum distance the Joystick can be pushed.

	private bool gotPosition;           // Joystick has a position.

	private int fingerID;               // ID of finger touching this Joystick.

	private int lastID;                 // ID of last finger touching this Joystick.

	private float tapTimer;             // Double-tap's timer.

	private bool enable;                // VJR external control.

	// @author Tapani
	public Texture2D aButton;        	// A button's Image
	public Texture2D bButton;        	// B button's Image
	private GameObject aButtonOBJ;         // aButton's Object.
	private GameObject bButtonOBJ;         // aButton's Object.
	private GUITexture aButtonBackground; // A button's GUI
	private GUITexture bButtonBackground; // B button's GUI
	private Vector2 aButtonPosition;     // Pixel Position in Screen-Space.
	private Vector2 bButtonPosition;     // Pixel Position in Screen-Space.
	private bool aButtonDown;	// Joystick has a position.
		
		
	//

    

	public void DisableJoystick ()
	{
		enable = false;
		ResetJoystick ();
	}

	public void EnableJoystick ()
	{
		enable = true;
		ResetJoystick ();
	}

    

	//

 

	private void ResetJoystick ()
	{

		VJRvector = new Vector2 (0, 0);
		VJRnormals = VJRvector;

		lastID = fingerID;
		fingerID = -1;
		tapTimer = 0.150f;

		joystick.color = inactiveColor;
		position = origin;
		gotPosition = false;
		
		// @author Tapani
		SendMessageUpwards ("VJRupdate", VJRnormals);

	}

	private Vector2 GetRadius (Vector2 midPoint, Vector2 endPoint, float maxDistance)
	{

		Vector2 distance = endPoint;

		if (Vector2.Distance (midPoint, endPoint) > maxDistance) {

			distance = endPoint - midPoint;
			distance.Normalize ();

			return (distance * maxDistance) + midPoint;

		}

		return distance;

	}
	
	//@author Tapani
	private void CheckButtons (){
		foreach (Touch touch in Input.touches) {
			int id = touch.fingerId;
//			Debug.Log("--------------------");
//			Debug.Log("Input.touchCount: "+ Input.touchCount);
//			Debug.Log("id: "+ id);
			
			float buttonOffset = aButtonBackground.pixelInset.width / 2;
			
			float touchLocationX = Input.GetTouch (id).position.x - buttonOffset;
			float touchLocationY = Input.GetTouch (id).position.y - buttonOffset;
			float touchAreaWidth = aButtonBackground.pixelInset.width * 0.50f;
			Debug.Log("touchAreaWidth "+touchAreaWidth);

			// Touches aButton
			if (id < Input.touchCount 
				&& (Mathf.Abs(touchLocationX - aButtonBackground.pixelInset.x) <= touchAreaWidth) 
				&& (Mathf.Abs(touchLocationY - aButtonBackground.pixelInset.y) <= touchAreaWidth) 
				&& Input.GetTouch (id).phase == TouchPhase.Began) {
			
	//			aButtonBackground.color = inactiveColor;
				SendMessageUpwards ("VJaButton");
	//			aButtonDown = false;
		
				// Touches the button area
	//					aButtonPosition = Input.GetTouch (fingerID).position;
				
	//			aButtonBackground.texture = aButton;
//				aButtonBackground.color = activeColor;
	//			aButtonDown = true;
//				Debug.Log("touch pos " + touch.position );
//				Debug.Log("button ");
			}
			
			// Touches bButton
			else if (id < Input.touchCount 
				&& (Mathf.Abs(touchLocationX - bButtonBackground.pixelInset.x) <= bButtonBackground.pixelInset.width) 
				&& (Mathf.Abs(touchLocationY - bButtonBackground.pixelInset.y) <= bButtonBackground.pixelInset.width) 
				&& Input.GetTouch (id).phase == TouchPhase.Began) {
			
	//			aButtonBackground.color = inactiveColor;
				SendMessageUpwards ("VJbButton");
	//			aButtonDown = false;
		
				// Touches the button area
	//					aButtonPosition = Input.GetTouch (fingerID).position;
				
	//			aButtonBackground.texture = aButton;
//				aButtonBackground.color = activeColor;
	//			aButtonDown = true;
//				Debug.Log("touch pos " + touch.position );
//				Debug.Log("button ");
			}
			
			
		}

	}

	private void GetPosition ()
	{

		foreach (Touch touch in Input.touches) {

			fingerID = touch.fingerId;
			



			if (fingerID >= 0 && fingerID < Input.touchCount) {
				
				// Touches the joystick area
				if (Input.GetTouch (fingerID).position.x < Screen.width / 3 && Input.GetTouch (fingerID).position.y < Screen.height / 3 && Input.GetTouch (fingerID).phase == TouchPhase.Began) {

					position = Input.GetTouch (fingerID).position;
					origin = position;

					joystick.texture = joystick2D;
					joystick.color = activeColor;

					background.texture = background2D;
					background.color = activeColor;

					if (fingerID == lastID && tapTimer > 0) {
						VJRdoubleTap = true;
					}
					gotPosition = true;
					
					Debug.Log("joystick ");

				} 
			}

		}

	}

	private void GetConstraints ()
	{

		if (origin.x < (background.pixelInset.width / 2) + 25) {
			origin.x = (background.pixelInset.width / 2) + 25;
		}

		if (origin.y < (background.pixelInset.height / 2) + 25) {
			origin.y = (background.pixelInset.height / 2) + 25;
		}

		if (origin.x > Screen.width / 3) {
			origin.x = Screen.width / 3;
		}

		if (origin.y > Screen.height / 3) {
			origin.y = Screen.height / 3;
		}

	}

	private Vector2 GetControls (Vector2 pos, Vector2 ori)
	{

		Vector2 vector = new Vector2 ();

		if (Vector2.Distance (pos, ori) > 0) {
			vector = new Vector2 (pos.x - ori.x, pos.y - ori.y);
		}

		return vector;

	}

 

	//

    

	private void Awake ()
	{

		gameObject.transform.localScale = new Vector3 (0, 0, 0);

		gameObject.transform.position = new Vector3 (0, 0, 999);

		if (Screen.width > Screen.height) {
			size = Screen.height;
		} else {
			size = Screen.width;
		}
		VJRvector = new Vector2 (0, 0);
		
		// @author Tapani
		joystickOBJ = new GameObject ("VJR-Joystick");
		joystickOBJ.transform.localScale = new Vector3 (0, 0, 0);

		joystick = joystickOBJ.AddComponent ("GUITexture") as GUITexture;

		joystick.texture = joystick2D;
		joystick.color = inactiveColor;

		backOBJ = new GameObject ("VJR-Joystick Back");

		backOBJ.transform.localScale = new Vector3 (0, 0, 0);

		background = backOBJ.AddComponent ("GUITexture") as GUITexture;

		background.texture = background2D;
		background.color = inactiveColor;
		
		// @author Tapani
		aButtonOBJ = new GameObject ("VJ-aButton");
		aButtonOBJ.transform.localScale = new Vector3 (0, 0, 0);
		aButtonBackground = aButtonOBJ.AddComponent ("GUITexture") as GUITexture;
		aButtonBackground.texture = aButton;
		aButtonBackground.color = inactiveColor;
		aButtonDown = false;
		aButtonPosition = new Vector3(Screen.width / 1.1f ,Screen.height / 6f,0);
		
		
		bButtonOBJ = new GameObject ("VJ-bButton");
		bButtonOBJ.transform.localScale = new Vector3 (0, 0, 0);
		bButtonBackground = bButtonOBJ.AddComponent ("GUITexture") as GUITexture;
		bButtonBackground.texture = bButton;
		bButtonBackground.color = inactiveColor;

		bButtonPosition = new Vector3(Screen.width / 1.35f ,Screen.height / 6f,0);
	
		

		fingerID = -1;
		lastID = -1;
		VJRdoubleTap = false;
		tapTimer = 0;
		length = 50;

		position = new Vector2 ((Screen.width / 3) / 2, (Screen.height / 3) / 2);
		origin = position;

		gotPosition = false;
		EnableJoystick ();
		enable = true;

	}

	private void Update ()
	{

		if (tapTimer > 0) {
			tapTimer -= Time.deltaTime;
		}

		if (fingerID > -1 && fingerID >= Input.touchCount) {
			ResetJoystick ();
		}

		if (enable == true) {

			if (Input.touchCount > 0 && gotPosition == false) {
				GetPosition ();
				GetConstraints ();
			}
			if (Input.touchCount > 0) {
				CheckButtons();
			}

			if (Input.touchCount > 0 && fingerID > -1 && fingerID < Input.touchCount && gotPosition == true) {

				foreach (Touch touch in Input.touches) {

					if (touch.fingerId == fingerID) {

						position = touch.position;
						position = GetRadius (origin, position, length);

						VJRvector = GetControls (position, origin);
						VJRnormals = new Vector2 (VJRvector.x / length, VJRvector.y / length);

						if (Input.GetTouch (fingerID).position.x > (Screen.width / 3) + background.pixelInset.width

                        || Input.GetTouch (fingerID).position.y > (Screen.height / 3) + background.pixelInset.height) {
							ResetJoystick ();
						}

						//
						
						// @author Tapani
						SendMessageUpwards ("VJRupdate", VJRnormals);
						

//						Debug.Log ("Joystick Axis:: " + VJRnormals); //<-- Delete this line | (X,Y), from -1.0 to +1.0 | Use this value "VJRnormals" in your scripts.

					}

				}

			}

			if (gotPosition == true && Input.touchCount > 0 && fingerID > -1 && fingerID < Input.touchCount) {

				if (Input.GetTouch (fingerID).phase == TouchPhase.Ended || Input.GetTouch (fingerID).phase == TouchPhase.Canceled) {
					ResetJoystick ();
				}

			}

			if (gotPosition == false && fingerID == -1 && tapTimer <= 0) {
				if (background.color != inactiveColor) {
					background.color = inactiveColor;
				}
			}
			
			// @author Tapani
//			if (aButtonDown == true && fingerID > -1 && tapTimer <= 0) {
////				if (aButtonBackground.color != inactiveColor) {
//					aButtonBackground.color = inactiveColor;
//					SendMessageUpwards ("VJRjump");
//					aButtonDown = false;
////				}
//			}

			background.pixelInset = new Rect (origin.x - (background.pixelInset.width / 2), origin.y - (background.pixelInset.height / 2), size / 5, size / 5);

			joystick.pixelInset = new Rect (position.x - (joystick.pixelInset.width / 2), position.y - (joystick.pixelInset.height / 2), size / 11, size / 11);
			
			// @author Tapani
			aButtonBackground.pixelInset = new Rect (aButtonPosition.x - (background.pixelInset.width / 2), aButtonPosition.y - (background.pixelInset.height / 2), size / 5, size / 5);
			
			bButtonBackground.pixelInset = new Rect (bButtonPosition.x - (background.pixelInset.width / 2), aButtonPosition.y - (background.pixelInset.height / 2), size / 5, size / 5);
			


		} else if (background.pixelInset.width > 0) {
			background.pixelInset = new Rect (0, 0, 0, 0);
			joystick.pixelInset = new Rect (0, 0, 0, 0);
		}
		


	}

}