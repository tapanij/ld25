#pragma strict
// Player's ragdoll handling
class PlayerRagdoll extends MonoBehaviour {
	function Start () {
		// Destroy the ragdoll after 3 seconds
		Destroy(transform.parent.gameObject,3);
		
		// TODO. This is just a quick fix to simulate knockback after getting hit by the bike
//		rigidbody.AddForce(Vector3(10,0,0),ForceMode.Impulse);
	}
}