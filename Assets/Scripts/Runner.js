#pragma strict

private var motor : CharacterMotor;
public var idleAnimation : AnimationClip;
public var jumpPoseAnimation : AnimationClip;
public var jumpAnimationSpeed : float = 1.15;
public var landAnimationSpeed : float = 1.0;
public var runAnimation : AnimationClip;
private var _animation : Animation;
private var jumpingReachedApex = false;
// Last time we performed a jump
private var lastJumpTime = -1.0;
private var jumpRepeatTime = 0.05;
// The last collision flags returned from controller.Move
private var collisionFlags : CollisionFlags; 
private var moveDirection = Vector3.zero;
var canJump = true;
private var xDirection: float;
private var zDirection: float;
private var randomXDirection: float;
private var randomZDirection: float;
public var jumpPower: float;
//public var speed : float = 3f;
private var walkAmount : float;
private var jumpInterval: float;
public var jumpIntervalMinimum: float = 10f;
public var jumpIntervalMaximum: float = 20f;
public var audioIntervalMinimum: float = 1f;
public var audioIntervalMaximum: float = 10f;
//public var player: FPSInputController;
public var walkMaxAnimationSpeed : float = 0.75;
public var trotMaxAnimationSpeed : float = 1.0;
public var runMaxAnimationSpeed : float = 1.0;
public var ragdoll : Transform;

enum EnemyState {
	Idle = 0,
	Walking = 1,
	Trotting = 2,
	Running = 3,
	Jumping = 4,
}

private var _enemyState : EnemyState;

function Awake () {

//	player = UnityEngine.GameObject.Find ("Player").GetComponent(FPSInputController);  
	moveDirection = transform.TransformDirection(Vector3.forward);
	motor = GetComponent(CharacterMotor);
	motor.inputJump = false;	
	
	_animation = GetComponent(Animation);
	if(!_animation)
		Debug.Log("The character you would like to control doesn't have animations. Moving her might look weird.");
	
//	var audioInterval = Random.Range(audioIntervalMinimum,audioIntervalMaximum);
//	Invoke("PlayAudio",audioInterval);
	

	if(!idleAnimation) {
		_animation = null;
		Debug.Log("No idle animation found. Turning off animations.");
	}
//	if(!walkAnimation) {
//		_animation = null;
//		Debug.Log("No walk animation found. Turning off animations.");
//	}
	if(!runAnimation) {
		_animation = null;
		Debug.Log("No run animation found. Turning off animations.");
	}
//	if(!jumpPoseAnimation && canJump) {
//		_animation = null;
//		Debug.Log("No jump animation found and the character has canJump enabled. Turning off animations.");
//	}

	
	//_enemyState = EnemyState.Jumping;
}

function Start () {

}

function EnemyMovement() {
	
	var controller : CharacterController = GetComponent(CharacterController);
	
	/*
		// Get the input vector from kayboard or analog stick
	var directionVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
	
	if (directionVector != Vector3.zero) {
		// Get the length of the directon vector and then normalize it
		// Dividing by the length is cheaper than normalizing when we already have the length anyway
		var directionLength = directionVector.magnitude;
		directionVector = directionVector / directionLength;
		
		// Make sure the length is no bigger than 1
		directionLength = Mathf.Min(1, directionLength);
		
		// Make the input vector more sensitive towards the extremes and less sensitive in the middle
		// This makes it easier to control slow speeds when using analog sticks
		directionLength = directionLength * directionLength;
		
		// Multiply the normalized direction vector by the modified length
		directionVector = directionVector * directionLength;
	}
	
	// Apply the direction to the CharacterMotor
	//motor.inputMoveDirection = directionVector;
	if (directionVector != Vector3.zero) {
			transform.rotation = Quaternion.LookRotation (directionVector);
	}
	*/
	
	
	//moveDirection = Vector3.RotateTowards(moveDirection, motor.inputMoveDirection, 500.0 * Mathf.Deg2Rad * Time.deltaTime, 1000);
	//moveDirection = moveDirection.normalized;

	//motor.inputJump = Input.GetButtonDown ("Jump");
	
	Movement();
	
	//ApplyJumping ();

    // Rotate around y - axis
    //transform.Rotate(0, Input.GetAxis ("Horizontal") * rotateSpeed, 0);
    
	
	
	// ANIMATION sector
	if(_animation) {
		if(_enemyState == EnemyState.Jumping) 
		{
			if(!jumpingReachedApex) {
				_animation[jumpPoseAnimation.name].speed = jumpAnimationSpeed;
				_animation[jumpPoseAnimation.name].wrapMode = WrapMode.ClampForever;
				_animation.CrossFade(jumpPoseAnimation.name);
			} else {
				_animation[jumpPoseAnimation.name].speed = -landAnimationSpeed;
				_animation[jumpPoseAnimation.name].wrapMode = WrapMode.ClampForever;
				_animation.CrossFade(jumpPoseAnimation.name);				
			}
		} 
		
		else 
		{
			if(controller.velocity.sqrMagnitude < 0.1) {
				_animation.CrossFade(idleAnimation.name,0.1f);
			}
			else 
			{
				if(_enemyState == EnemyState.Running) {
					_animation[runAnimation.name].speed = Mathf.Clamp(controller.velocity.magnitude, 0.0, runMaxAnimationSpeed);
					_animation.CrossFade(runAnimation.name);	
				}
//				else if(_enemyState == EnemyState.Trotting) {
//					_animation[walkAnimation.name].speed = Mathf.Clamp(controller.velocity.magnitude, 0.0, trotMaxAnimationSpeed);
//					_animation.CrossFade(walkAnimation.name);	
//				}
//				else if(_enemyState == EnemyState.Walking) {
//					_animation[walkAnimation.name].speed = Mathf.Clamp(controller.velocity.magnitude, 0.0, walkMaxAnimationSpeed);
//					_animation.CrossFade(walkAnimation.name);	
//				}
				
			}
			
		}
	}
	
		if(motor.grounded && _enemyState != EnemyState.Idle){
			_enemyState = EnemyState.Idle;
		}
		
}



/*
function ApplyJumping ()
{
	// Prevent jumping too fast after each other
	if (lastJumpTime + jumpRepeatTime > Time.time)
		return;

	if (motor.grounded) {
		SendMessage("DidJump", SendMessageOptions.DontRequireReceiver);
	}
}
*/

function DidJump ()
{
	//jumping = true;
	jumpingReachedApex = false;
	lastJumpTime = Time.time;
	//lastJumpStartHeight = transform.position.y;
	//lastJumpButtonTime = -10;
	
	_enemyState = EnemyState.Jumping;

}

/*
function Jump () {
	//if touches ground
	//timeToJump = false
	
	// Move forward / backward
    var forward : Vector3 = transform.TransformDirection(Vector3.forward);
    controller.SimpleMove(forward * speed);
}
*/

function PlayAudio()
	{
		if (audio) {
			if (!audio.isPlaying)
			audio.Play ();                          
		}
	}


/*****************/
/** BRAINS **/
/*****************/

function Movement()
	{
			/*
			var controller : CharacterController = GetComponent(CharacterController);
			
			if(walkAmount >= 5){
				RandomDirection();
			}
			
			var movementDirection = new Vector3(randomXDirection,0,randomZDirection);
			motor.inputMoveDirection = movementDirection;
			transform.rotation = Quaternion.LookRotation (movementDirection);
			motor.inputJump = true;
			walkAmount += Time.deltaTime;
			*/

//			if(motor.inputMoveDirection.x != 0 || motor.inputMoveDirection.y != 0 || motor.inputMoveDirection.z != 0)
//				transform.rotation = Quaternion.LookRotation (motor.inputMoveDirection);
			
		motor.inputMoveDirection = moveDirection;
		_enemyState = EnemyState.Running;	
	}

function RandomDirection ()
	{
		randomXDirection = Random.Range(-1,1);
		randomZDirection = Random.Range(-1,1);
		walkAmount = 0;
	}
	
//function JumpForward ()
//	{
//		CancelInvoke();
//		jumpInterval = Random.Range(jumpIntervalMinimum,jumpIntervalMaximum);
//		Invoke("JumpForward",jumpInterval);
//		var audioInterval = Random.Range(audioIntervalMinimum,audioIntervalMaximum);
//		Invoke("PlayAudio",audioInterval);
//		
//		var controller : CharacterController = GetComponent(CharacterController);
//
//		//xDirection = 2;
//		//zDirection = 1;
//		jumpPower = Random.Range(10.0f,20.0f);
//		var relativeDirection : Vector3;
//		
//		// Obstacles ahead?
//		// 2 = ignore raycast layer
//		//print(Vector3.Distance(transform.position, previousPosition));
//		//TODO this is a quickfix against tree collission
//		
//		/*
//		if(previousPosition != Vector3(0,0,0) && Vector3.Distance(transform.position, previousPosition) < 1f){
//			relativeDirection = transform.TransformDirection (Random.Range(-0.5f*jumpPower,0.5f*jumpPower), 0, jumpPower*-1f);
//		}
//	    else */
//	    
//	    if (!Physics.Raycast (transform.position, transform.TransformDirection (Vector3.forward), jumpPower)) {
//	        relativeDirection = transform.TransformDirection (Random.Range(-0.5f*jumpPower,0.5f*jumpPower), 0, jumpPower);
//	    }
//	    else if (!Physics.Raycast (transform.position, transform.TransformDirection (Vector3.left), jumpPower)) {
//	    	//print("nothing at left");
//	        relativeDirection = transform.TransformDirection (jumpPower*-1f, 0, Random.Range(-0.5f*jumpPower,0.5f*jumpPower));
//	    }
//	    else if (!Physics.Raycast (transform.position, transform.TransformDirection (Vector3.back), jumpPower)) {
//	        //print("nothing at back");
//	        relativeDirection = transform.TransformDirection (Random.Range(-0.5f*jumpPower,0.5f*jumpPower), 0, jumpPower*-1f);
//	    }
//	    else if (!Physics.Raycast (transform.position, transform.TransformDirection (Vector3.right), jumpPower)) {
//	    	//print("nothing at right");
//	        relativeDirection = transform.TransformDirection (jumpPower, 0, Random.Range(-0.5f*jumpPower,0.5f*jumpPower));
//	    }
//	    else
//	    	  relativeDirection = transform.TransformDirection (Random.Range(-0.5f*jumpPower,0.5f*jumpPower), 0, jumpPower);
//		
//		previousPosition = transform.position;
//		
//		if(relativeDirection != Vector3(0,0,0)){
//				motor.inputMoveDirection = relativeDirection;
//				motor.inputJump = true;
//			}
//			
//
//	}
//
//function JumpTowards ()
//	{
//			var hit: RaycastHit;
//			var rayDirection: Vector3 = flyStatue.transform.position - transform.position;
//			
//			//print("hit" + distance);
//			CancelInvoke();
//			jumpInterval = Random.Range(jumpIntervalMinimum,jumpIntervalMaximum);
//			Invoke("JumpForward",jumpInterval);
//			var audioInterval = Random.Range(audioIntervalMinimum,audioIntervalMaximum);
//			Invoke("PlayAudio",audioInterval);
//			
//			var controller : CharacterController = GetComponent(CharacterController);
//	
//			//xDirection = 2;
//			//zDirection = 1;
//			jumpPower = Random.Range(10.0f,20.0f);
//			//var relativeDirection : Vector3;
//			//relativeDirection = transform.TransformDirection (Random.Range(-0.5f*jumpPower,0.5f*jumpPower), 0, jumpPower);
//			//var tempDirection: Vector3 = transform.position - flyStatue.transform.position;
//			
//			if(rayDirection != Vector3(0,0,0)){
//				Debug.DrawLine (transform.position, flyStatue.transform.position);
//				motor.inputMoveDirection = rayDirection;
//				motor.inputJump = true;
//			}
//	}

public function Die(){
	if(ragdoll != null){
		Destroy(gameObject);
		var theClonedRagdoll : Transform;
	    theClonedRagdoll = Instantiate(ragdoll,transform.position, transform.rotation);
	    ragdoll = null;
	}
}

@script RequireComponent(CharacterController)
@script RequireComponent (CharacterMotor)