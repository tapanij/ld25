#pragma strict
// This script makes sure that the shadow projector doesn't rotate
class ShadowProjector extends MonoBehaviour {
	private var projectorRotation: Quaternion;
	
	function Start () {
		transform.eulerAngles = Vector3(90, 90, 0);
		projectorRotation = transform.rotation;
	}
	
	function FixedUpdate () {
		if(projectorRotation != transform.rotation){
			transform.rotation = projectorRotation;
		}
	}
}