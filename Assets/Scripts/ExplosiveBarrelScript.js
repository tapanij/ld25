#pragma strict

// Throwable barrels respawn when needed to
class ExplosiveBarrelScript extends Barrel {
	public var activated: boolean = false;
	private var radius: float = 10.0F;
	private var power: float = 600.0F;
	
	// Explosion
	function OnCollisionEnter(collision: Collision) {
		if(activated){
	//	    if (collision.collider.CompareTag("Player") && collision.relativeVelocity.magnitude > 6.5f) {
	//	        SendMessageUpwards("LostLife");
	//	    }
	
			Debug.Log("KABOOM!");

			checkEnemyColliders();
			explosion();
		}
		activated = false;
	}
	
	function checkEnemyColliders(){
		var colliders: Collider[]  = Physics.OverlapSphere (transform.position, radius);
	    for(var col: Collider in colliders){
	    Debug.Log("col.tag" + col.tag);
	       if(col.CompareTag("Runner")){
	           	col.transform.GetComponent(Runner).Die();
	       }
	       else if(col.rigidbody){
	            if(col.rigidbody != gameObject.rigidbody){
	            	if(col.CompareTag("BreaksBarrel")){
	            		col.transform.GetComponent(BikeController).StopEngine();
	            	}
	            }
	        }
	    }
	}
	
	function explosion(){
		var colliders: Collider[]  = Physics.OverlapSphere (transform.position, radius);
	    for(var col: Collider in colliders){
	    Debug.Log("col.tag" + col.tag);
	         if(col.rigidbody){
	            if(col.rigidbody != gameObject.rigidbody){
	                col.rigidbody.AddExplosionForce (power, transform.position, radius);
	            }
	        }
	    }
	}
}