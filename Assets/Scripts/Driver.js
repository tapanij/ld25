#pragma strict
// Driver's ragdoll functions
class Driver extends MonoBehaviour {
	// The ragdoll version of the driver
	public var ragdoll : Transform;
	private var destroyDriver : int = 0;
	
	function OnTriggerEnter (other : Collider) {
		// Replace the static driver with a ragdoll driver if the static driver touches anything solid
		if(!other.isTrigger && ragdoll != null){
			Destroy(gameObject);
			var theClonedRagdoll : Transform;
		    theClonedRagdoll = Instantiate(ragdoll,transform.position, transform.rotation);
		    // Tell the bikeController to stop the bike
		    SendMessageUpwards("StopEngine");
		    ragdoll = null;
		}
	}
	
//	function FixedUpdate{
//	// Attempt to fix the double ragdoll bug
//		if(destroyDriver == 1){
//			
//			destroyDriver = 2; 
//		}
//	}
}