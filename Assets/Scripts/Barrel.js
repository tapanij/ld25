#pragma strict
// Throwable barrels respawn when needed to
class Barrel extends MonoBehaviour {
	// Save the first position of the barrel so we can respawn it later to the original position
	private var originalPosition: Vector3;
	private var originalRotation: Quaternion;
	
	function Start () {
		originalPosition = rigidbody.position;
		originalRotation = rigidbody.rotation;
	}
	
	function FixedUpdate () {
		// Reset the barrel's position after delay when it's not being carried(!isTrigger), is moved and not already been reseted
		if(originalPosition.x != rigidbody.position.x && !IsInvoking("ResetBarrel") && !collider.isTrigger){
			Invoke("ResetBarrel",5);
		}
	}
	
	function ResetBarrel () {
		rigidbody.velocity = Vector3.zero;
		rigidbody.angularVelocity = Vector3.zero;
		rigidbody.position = originalPosition;
		rigidbody.rotation = originalRotation;
	}
}